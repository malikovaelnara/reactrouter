import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {Route} from "react-router-dom";
import Web from "./Web";
import Graphic from "./Graphic";
import Support from "./Support";
import Design from "./Design";
import Marketing from "./Marketing";
import Seo from "./Seo";
import "./App.scss";
class App extends Component {
  render() {
    return (
        <>
        <ul className ="tabs">

          <li><Link to={'/webdesign'}>Web</Link></li>
          <li><Link to={'/graphicdesign'}>Graphic</Link></li>
            <li><Link to={'/onlinesupport'}>Support</Link></li>
            <li><Link to={'/appdesign'}>Design</Link></li>
            <li><Link to={'/onlinemarketing'}>Marketing</Link></li>
            <li><Link to={'/seoservices'}>Seo</Link></li>
        </ul>
            <Route path={'/webdesign'} component={Web}/>
            <Route path={'/graphicdesign'} component={Graphic}/>
            <Route path={'/onlinesupport'} component={Support}/>
            <Route path={'/appdesign'} component={Design}/>
            <Route path={'/onlinemarketing'} component={Marketing}/>
            <Route path={'/seoservices'} component={Seo}/>
        </>

    );
  }
}

export default App;
